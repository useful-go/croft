package croft

import "io"
import "os"
import "time"
import "encoding/binary"
import "bytes"
import "math/rand"
import "hash/crc32"

// BlockSize is the size of the body of a Croft block.
const BlockSize = 4096

// HeaderSize is the size of the header of a Croft block.
const HeaderSize = 48

// FooterSize is the size of the footer of a Croft block.
const FooterSize = HeaderSize

// BodySize is the size of the body of a Croft block.
const BodySize = BlockSize - HeaderSize - FooterSize

// Error is the error type for Croft.
type Error string

// Error is an error
func (e Error) Error() string {
	return string(e)
}

// Implementation assertion
var _ error = Error("")

const (
	ErrorNotFound    Error = "not found"
	ErrorBadCRC      Error = "bad crc"
	ErrorCannotWrite Error = "cannot write"
)

// Entry is an entry in the Croft file.
type Entry struct {
	// Any errors encountered when reading this entry
	Error error
	// Body of the entry
	Body []byte
	// Time is the timestamp of the entry. If not set time.Now() is used.
	Time *time.Time
	// Wipe is true if the entry was wiped.
	Wipe bool
	// amount of blocks this entry needs for storage.
	Blocks int64
}

// Option is the options for opening a Croft File
type Option struct {
	// SyncEvery is how often the backgriound thread should use sync.
	// If 0 or less, 100 is used
	SyncEvery int
}

// low level block information as header and footer
type blockInfo struct {
	Magic     [4]byte // CRV1
	Status    [4]byte // GOOD, WIPE, BADB
	CRCInfo   uint32  // CRC32 of the blockInfo itself, excluding the CRC
	CRCBody   uint32  // CRC32 of the body itself, excluding the header
	Sec       int64   // unix timestamp seconds of the entry
	Nsec      int64   // timestamp nanoseconds of the entry
	EntrySize int64   // Actual size of entry in bytes.
	Reserved  int64   // Reserved, set to 0.
}

// low level blocks
type block struct {
	Header blockInfo
	Body   [BodySize]byte
	Footer blockInfo
}

func entryBlockSize(entrySize int64) int64 {
	res := entrySize / BodySize
	mod := entrySize % BodySize
	if mod != 0 {
		res++
	}
	return res
}

func (b block) entryBlockSize() int64 {
	return entryBlockSize(b.Header.EntrySize)
}

func (bi blockInfo) calculateCrc() uint32 {
	crc := crc32.New(crc32.MakeTable(crc32.Koopman))
	binary.Write(crc, binary.LittleEndian, bi.Magic)
	binary.Write(crc, binary.LittleEndian, bi.Status)
	binary.Write(crc, binary.LittleEndian, bi.CRCBody)
	binary.Write(crc, binary.LittleEndian, bi.Sec)
	binary.Write(crc, binary.LittleEndian, bi.Nsec)
	binary.Write(crc, binary.LittleEndian, bi.EntrySize)
	binary.Write(crc, binary.LittleEndian, bi.Reserved)
	return crc.Sum32()
}

func (bi blockInfo) validate() error {
	crc := bi.calculateCrc()
	if crc != bi.CRCInfo {
		return ErrorBadCRC
	}
	return nil
}

func (b block) calculateCrcBody() uint32 {
	crc := crc32.New(crc32.MakeTable(crc32.Koopman))
	binary.Write(crc, binary.LittleEndian, b.Body)
	return crc.Sum32()
}

func (b block) validate() error {
	headErrs := b.Header.validate()
	if headErrs != nil {
		return headErrs
	}
	footErrs := b.Footer.validate()
	if footErrs != nil {
		return footErrs
	}
	crc := b.calculateCrcBody()
	if crc != b.Header.CRCBody {
		return ErrorBadCRC
	}

	return nil
}

type Croft struct {
	// Name of the croft file
	name string
	// Appender to append records to.
	appender *os.File
	// Options of this file
	option Option
	// Sequencing of entries the back end thread has to write
	entries chan (Entry)
	// errors is a channel on which detected errors are sent
	errors chan (error)
}

func blocksToEntry(blocks []block) (Entry, error) {
	var entry Entry
	if len(blocks) < 1 {
		entry.Error = ErrorNotFound
		return entry, ErrorNotFound
	}
	header := blocks[0].Header
	tm := time.Unix(header.Sec, header.Nsec)
	entry.Time = &tm
	entry.Blocks = entryBlockSize(header.EntrySize)
	entry.Wipe = bytes.Equal(header.Status[:], []byte("WIPE"))
	entry.Body = []byte{}

	for _, b := range blocks {
		entry.Body = append(entry.Body, b.Body[:]...)
	}
	entry.Body = entry.Body[:header.EntrySize]
	return entry, nil
}

func entryToBlocks(entry Entry) []block {
	blocks := []block{}
	entrySize := int64(len(entry.Body))
	blocksNeeded := entryBlockSize(entrySize)
	if entry.Time == nil {
		tm := time.Now()
		entry.Time = &tm
	}

	for i := int64(0); i < blocksNeeded; i++ {
		b := block{}
		bodyStart := i * BodySize
		bodyEnd := (i + 1) * BodySize
		if bodyEnd > entrySize {
			bodyEnd = entrySize
		}
		copy(b.Body[:], entry.Body[bodyStart:bodyEnd])
		copy(b.Header.Magic[:], []byte("CRV1"))
		copy(b.Header.Status[:], []byte("GOOD"))
		if entry.Wipe {
			copy(b.Header.Status[:], []byte("WIPE"))
			rand.Read(b.Body[:])
		}

		b.Header.CRCBody = b.calculateCrcBody()
		b.Header.Sec = entry.Time.Unix()
		b.Header.Nsec = int64(entry.Time.Nanosecond())
		b.Header.EntrySize = entrySize
		b.Header.Reserved = 0
		b.Header.CRCInfo = b.Header.calculateCrc()
		b.Footer = b.Header
		blocks = append(blocks, b)
	}
	return blocks
}

// position in the file. Returns -1 on error
func tell(f *os.File) int64 {
	if f == nil {
		return -1
	}
	res, err := f.Seek(0, io.SeekCurrent)
	if err != nil {
		return -1
	}
	return res
}

func seekBlock(f *os.File, blockOffset int64, whence int) (int64, error) {
	offset, err := f.Seek(blockOffset*BlockSize, whence)
	return offset, err
}

func readCurrentBlock(f *os.File) (block, error) {
	b := block{}
	err := binary.Read(f, binary.LittleEndian, &b)
	if err != nil {
		return b, err
	}
	return b, b.validate()
}

func writeCurrentBlock(f *os.File, b block) error {
	err := binary.Write(f, binary.LittleEndian, b)
	if err != nil {
		return err
	}
	return f.Sync()
}

func readCurrentEntry(f *os.File) (Entry, error) {
	var entry Entry
	b, err := readCurrentBlock(f)
	if err != nil {
		return entry, err

	}
	blocks := []block{b}
	end := b.entryBlockSize()
	for i := int64(1); i < end; i++ {
		b, err := readCurrentBlock(f)
		if err != nil {
			return entry, err
		}
		blocks = append(blocks, b)
	}
	return blocksToEntry(blocks)
}

// Returns ErrorNotFound if not found.
func findEntry(f *os.File, sec int64) (Entry, error) {
	var entry Entry

	last, err := seekBlock(f, 0, io.SeekEnd)
	if err != nil {
		return entry, err
	}
	offset, err := seekBlock(f, 0, io.SeekStart)
	if err != nil {
		return entry, err
	}

	// This is a simple semi-linear scan, but Croft is indended as a log file,
	// not for very quick lookups.
	for offset < last {
		entry, err := readCurrentEntry(f)
		if err != nil {
			return entry, err
		}
		if entry.Time.Unix() == sec {
			return entry, nil
		}
		offset += BlockSize
	}
	return entry, ErrorNotFound
}

func writeCurrentEntry(f *os.File, entry Entry) (Entry, error) {
	blocks := entryToBlocks(entry)
	for _, b := range blocks {
		err := writeCurrentBlock(f, b)
		if err != nil {
			entry.Error = err
			return entry, err
		}
	}

	return entry, nil
}

func seekPreviousEntry(f *os.File) (err error) {
	_, err = seekBlock(f, -1, io.SeekCurrent)
	if err != nil {
		return err
	}
	lastBlock, err := readCurrentBlock(f)
	if err != nil {
		return err
	}
	entrySize := lastBlock.Header.EntrySize
	if entrySize > 1 {
		_, err := seekBlock(f, -(entrySize - 1), io.SeekCurrent)
		if err != nil {
			return err
		}
	}
	return nil
}

func appendEntry(f *os.File, entry Entry) (Entry, error) {
	_, err := seekBlock(f, 0, io.SeekEnd)
	if err != nil {
		return entry, err
	}
	return writeCurrentEntry(f, entry)
}

func readLastEntry(f *os.File, entry Entry) (Entry, error) {
	_, err := seekBlock(f, -BlockSize, io.SeekEnd)
	if err != nil {
		entry.Error = err
		return entry, err
	}
	return readCurrentEntry(f)
}

func (c *Croft) closeCroft() error {
	var serr, cerr error
	if c.appender != nil {
		close(c.entries)
		serr = c.appender.Sync()
		cerr = c.appender.Close()
		c.appender = nil
		if serr == nil {
			return cerr
		}
		return serr
	}
	return nil
}

func (c *Croft) background() {
	var err error
	for entry := range c.entries {
		_, err = appendEntry(c.appender, entry)
		c.errors <- err
	}
}

// OpenFile opens a os.File as a Croft. Sends errors to Croft.Error on error.
// The file should be opened for appending and writing only.
func OpenFile(file *os.File, name string, option Option) *Croft {
	croft := &Croft{
		name:     name,
		errors:   make(chan (error)),
		appender: file,
		option:   option,
		entries:  make(chan (Entry)),
	}
	// Start background thread
	go croft.background()
	return croft
}

// Close requests file closure asynchronously.
// Use the Wait function to wait for finishing and to
// check for any errors.
func (c *Croft) Close() error {
	return c.closeCroft()
}

// Append appends an entry asynchronously.
// Use the Wait function to wait for finishing and to
// check for any errors.
func (c *Croft) Append(entry Entry) {
	c.entries <- entry
}

// Wait waits for a write operation to end and returns any errors
func (c *Croft) Wait() error {
	err := <-c.errors
	return err
}

// Open opens a named file with the given options
func Open(name string, option Option) (*Croft, error) {
	file, err := os.OpenFile(name, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0o700)
	if err != nil {
		return nil, err
	}
	return OpenFile(file, name, option), nil
}

// Walker allows to walk through over the Croft file.
// Each walker opens a handle to the file, so you should not use too many
// cursors.
type Walker struct {
	CanWrite bool
	name     string
	option   Option
	file     *os.File
}

// Walker returns a walker for the Croft file or an error.
// The walker opens a file descriptor, so it should be closed with
// Walker.Close.
func (c *Croft) Walker(canWrite bool) (*Walker, error) {
	return NewWalker(c.name, canWrite, c.option)
}

// NewWalker returns a walker for the named Croft file or an error.
// The walker opens a file descriptor, so it should be closed with
// Walker.Close.
func NewWalker(name string, canWrite bool, option Option) (*Walker, error) {
	flags := os.O_RDONLY
	if canWrite {
		flags = os.O_RDWR
	}
	file, err := os.OpenFile(name, flags, 0o700)
	if err != nil {
		return nil, err
	}
	_, err = file.Seek(0, io.SeekStart)

	walker := &Walker{
		name:     name,
		option:   option,
		file:     file,
		CanWrite: canWrite,
	}
	return walker, nil
}

// Close closes the walker.
func (w *Walker) Close() {
	if w != nil && w.file != nil {
		w.file.Close()
		w.file = nil
	}
}

// Next returns the next entry if any.
func (c *Walker) Next() *Entry {
	entry, err := readCurrentEntry(c.file)
	if err == io.EOF {
		return nil
	}
	entry.Error = err
	return &entry
}

// Prev returns the previous entry if any.
func (c *Walker) Prev() *Entry {
	err := seekPreviousEntry(c.file)
	if err != nil {
		return nil
	}
	entry, err := readCurrentEntry(c.file)
	if err == io.EOF {
		return nil
	}
	entry.Error = err
	err = seekPreviousEntry(c.file)
	if err != nil {
		return nil
	}
	return &entry
}

// Wipe wipes the entry the last call to Next returned. Only allowed if this
// cursor can write. The delete does not remove the entry but is is cleared
// out and will be returned with the Wipe flag set to true on next iterations.
func (c *Walker) Wipe() error {
	if !c.CanWrite {
		return ErrorCannotWrite
	}
	err := seekPreviousEntry(c.file)
	if err != nil {
		return err
	}
	entry := Entry{Wipe: true}
	_, err = writeCurrentEntry(c.file, entry)
	return err
}
