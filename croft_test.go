package croft

import "os"
import "fmt"
import "testing"
import "github.com/matryer/is"

const testCroft = "testdata/test.croft"
const testMulti = "testdata/test.multi"

func TestOpen(t *testing.T) {
	is := is.New(t)
	croft, err := Open(testCroft, Option{})
	is.NoErr(err)
	defer croft.Close()
	defer os.Remove(testCroft)
}

func TestAppend(t *testing.T) {
	is := is.New(t)
	croft, err := Open(testCroft, Option{})
	defer croft.Close()
	defer os.Remove(testCroft)

	is.NoErr(err)
	testText := ""
	for i := 0; i < 1000; i++ {
		testText = fmt.Sprintf("%s\nThis is line nr %04d", testText, i)
	}

	entry := Entry{Body: []byte(testText)}

	croft.Append(entry)
	err = croft.Wait()
	is.NoErr(err)

}

func TestAppendNoWait(t *testing.T) {
	is := is.New(t)
	croft, err := Open(testCroft, Option{})
	defer os.Remove(testCroft)

	is.NoErr(err)
	testText := ""
	for i := 0; i < 1000; i++ {
		testText = fmt.Sprintf("%s\nThis is line nr %04d", testText, i)
	}

	entry := Entry{Body: []byte(testText)}

	croft.Append(entry)
	croft.Close()

}

func TestWalker(t *testing.T) {
	is := is.New(t)
	croft, err := Open(testCroft, Option{})
	defer croft.Close()
	defer os.Remove(testCroft)

	is.NoErr(err)
	testText := ""
	for i := 0; i < 100; i++ {
		testText = fmt.Sprintf("This is line nr %04d", i)
		entry := Entry{Body: []byte(testText)}
		croft.Append(entry)
		err = croft.Wait()
		is.NoErr(err)
	}

	walker, err := croft.Walker(false)
	is.NoErr(err)

	for i := 0; i < 100; i++ {
		testText = fmt.Sprintf("This is line nr %04d", i)
		entry := walker.Next()
		is.True(entry != nil)
		is.True(entry.Time != nil)
		is.Equal(testText, string(entry.Body))
	}

}
