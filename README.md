# CROFT

# Introduction

CROFT is a Corruption Resistant Output File Toolkit, licensed under the MIT
license.

CROFT files are append only files suitable for logging or historical
data series. They have several features to make them resilient against
corruption. Even if a record is corrupted you can still read the other
ones.

It is not possible to delete entries from the CROFT file, but is is possible
to wipe entries, which overwrites them with blanks, for privacy purposes.

# Entries

Data is stored in croft by appending Entries to the CROFT file. Only for
clearing entries, the file is written in the middle.

The structure of entries in Go is as follows

type Entry struct {
	// Any errors encountered when reading this entry
	Error error
	// Body of the entry
	Body []byte
	// Time is the timestamp of the entry. If not set time.Now() is used.
	Time *time.Time
	// Wipe is true if the entry was wiped.
	Wipe bool
	// amount of blocks this entry needs for storage.
	Blocks int64
}

Only Body needs to be filled in when appending an entry.

# Disk layout Layout

Croft files consist of 4kb blocks, to match the Linux block size.
Each block has a 48 byte header and a 4000 bytes content, followed
by a repeat of the header for redundancy. The header is stored in
little endian encoding.

An entry consists of at least 1 block. The if entry to be stored has more
data then fits in 1 block, then additional blocks will be written as
several blocks with the same entry ID.

The header consists of the following fields:

- Magic: [4]byte CRV1
- Status:  [4]byte: GOOD, WIPE, BADB
- CRC: [8]byte CRC64 using the ISO polynomial of other header fields
  after this one and the block body.
- Sec [8]byte 			-> int64 Unix seconds timestamp
- Nsec [8]byte 			-> int64 Unix nanoseconds timestamp
- Entry: [8]byte 		-> int64 Entry ID the block belongs to.
- EntrySize: [8]byte 	-> int64 Actual size of entry in bytes.
- Reserved: [8]byte     -> Reserved, set to 0.

For a GOOD block:
For a data block the 4000 bytes contain the payload of the message.

For a WIPE block:
The 4000 bytes contain random data to wipe the previous contents with.

For a BADB block: corruption of a DATA block or header was detected.
The data may be corrupted.

